<?php
include_once '/H_header.php';
include_once '../../../../vendor/autoload.php';

use Apps\Bitm\Seip129479\Mobile\Mobile;
$obj=new Mobile();
if(isset($_GET['id'])&& !empty($_GET['id']))
{
    $data=$obj->prepare($_GET)->show();
    
    if($_GET['id']!=$data['unique_id'])
    {
        $_SESSION['unauthorise_err']='<b style="color: red; font-size:16px;">Opps Not Found..!!!!!!!!!</b>';
        header('location:error.php');
    }
}

?>

<form action="update.php" method="post" style="height: 300px; margin: 0 auto;"><br><br><br>
    <table style=" margin: 0 auto; border: 2px solid #ff0000; padding: 20px;">
        
        <tr>
            <td style=""><input type="text" name="title" value="<?php if(isset($data['title'])) { echo $data['title']; }
?>"/></td>
            <td style=""><input type="text" name="laptop" value="<?php if(isset($data)) { echo $data['laptop_models']; }
?>"/></td>
            <td style=""><input type="hidden" name="id" value="<?php echo $data['id']?>"/></td>
            <td><input type="submit" value="Update"/></td>
        </tr>
         <tr>
             <td colspan="2">
         <center>
             <?php 
                if(isset($_SESSION['msg'])&& !empty($_SESSION['msg']))
                {
                    echo $_SESSION['msg'];
                    unset($_SESSION['msg']);
                }
             ?>
         </center>
                 
             </td>
        </tr>
        <tr>
            <td><a href="index.php"><i>Go back</i></a></td>
        </tr>
    </table>
</form>

<?php include_once './footer.php';?>