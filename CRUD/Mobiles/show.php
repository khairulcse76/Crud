<?php
include_once './Mobile.php';
$obj = new Mobile();
$singledata = $obj->prepare($_GET)->show();
?>

<a href="create.php">Add new</a>
<a href="index.php">View list</a>

<table border="1">
    <tr>
        <th>ID</th>
        <th>Model Name</th>
        <th colspan="3">Action</th>
    </tr>
    
    <tr>
            <td><?php echo $singledata['id'] ?></td>
            <td><?php echo $singledata['title'] ?></td>
            
            <td><a href="edit.php?id=<?php echo $singledata['id'] ?>">Edit</a> </td>
            <td><a href="delete.php?id=<?php echo $singledata['id'] ?>">Delete</a> </td>

    </tr>
    
</table>