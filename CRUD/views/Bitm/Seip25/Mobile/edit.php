<?php
include_once("../../../../vendor/autoload.php");
use Apps\Bitm\Seip25\Mobile\Mobile;

$obj = new Mobile();

if (isset($_GET['id']) && !empty($_GET['id'])) {
    $singledata = $obj->prepare($_GET)->show();
    if($_GET['id']!=$singledata['unique_id']){
    
        $_SESSION['St_Err'] = "Opps ! Something going wrong"."<br>"."<a href='index.php'>Go back to Index</a>";
        header('location:errors.php');
    }
    }

if (isset($_SESSION['Msg']) && !empty($_SESSION['Msg'])) {
    echo $_SESSION['Msg'];
    unset($_SESSION['Msg']);
}

//print_r($singledata);
?>
<a href="index.php">Back to list</a>
<html>
<head>
    <title>Mobile Model</title>
</head>
<body>
<fieldset>
    <legend>Update favorite mobile models.</legend>
    <form action="update.php" method="post">
        <label>Enter Favorite Mobile Model</label>
        <input type="text" name="title" value="<?php if (isset($singledata)) {
            echo $singledata['title'];
        } ?>">
        <input type="hidden" name="id" value="<?php echo $singledata['unique_id'] ?>">
        <input type="submit" value="Update">
    </form>
</fieldset>
</body>
</html>